variable "cidr_block" {
  default = "10.10.10.0/23"
}

variable "region" {
  default = "us-east-1"
}

variable "profile" {
  default = "cino-dev"
}

variable "vpc_name" {
  default = "HOOQ-TEST-VPC"
}
