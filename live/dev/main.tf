# ------------------------------------------------------
#  Dev environment settings
# ------------------------------------------------------

provider "aws" {
  region  = var.region
  profile = var.profile
  version = "~> 2.0"
}

terraform {
  backend "s3" {
    bucket = "hooq-sretest-state"
    key    = "statefile"
    region = "us-east-1"
    profile = "cino-dev"
  }
}

module "hooq_vpc_module" {
  source = "../../modules/network"
}

module "hooq_storage" {
  source = "../../modules/storage"
  hooq_private_subnet_id = module.hooq_vpc_module.hooq_private_subnet_id
}

module "hooq_compute" {
  source = "../../modules/compute"
  hooq_cf-s3-domain-name = module.hooq_storage.hooq_s3_domain_name
  hooq_private_subnet_id = module.hooq_vpc_module.hooq_private_subnet_ids
  hooq_private_security_group = module.hooq_vpc_module.hooq_private_sg_id
}
