# HOOQ SRE Test
-------------------------------------------------------------------------------------------

Terraform source code for the [hooq sre test](https://github.com/HOOQTV/cloud_sre_test)

The source code is divided into

* module : All the reusable module code located here
* live   : All the environment specific details are located here

## Prerequisites

* S3 bucket for storing state file
* terraform 0.12
* AWS credentials under default location with a profile name


## Setup

Following variables need to be updated in the live/dev/main.tf

Variable name  | Description
-------------- | --------------
bucket         | S3 bucket to store the state file
key            | Key/path for the state file in the s3 bucket
region         | AWS region of the backend S3 bucket
profile        | AWS profile to use for accessing the backend bucket


## How to run?

The source code is capable of handling multiple environment. So let's say you want to execute in dev.

All the modules variables can be overrided in the live/<env>/variables.tf file


```bash
   cd live/dev/
   terraform init
   terraform plan
   terraform apply
```

### Destroy the deployment
```bash
terraform destroy
```

## Code promotion

To promote a code in this repo, we can use branching strategy create a feature branch to test new changes and update the module source code. Once tested merge the feature to the master to deploy to all the environments.

## Architecture 1
* [Solution 1](https://drive.google.com/open?id=1Amw07u8Fi2WP5_KhQWqZzdjOi972kTFM)
* [Solution 2](https://drive.google.com/open?id=1BE3TDCB0Bh6l4dL8n40_XESN3Ew1ppsF) -->Implemented in the code

## Reference
[Terraform Docs](https://www.terraform.io/docs/providers/aws)
