# -----------------------------------------------------------------
# Declare all the variable used in the module here
# ----------------------------------------------------------------

variable "region" {
  default = "us-east-1"
}

variable "hooq_cidr_block" {
  default =  "10.0.0.0/23"
}

variable "hooq_vpc_name" {
  default = "HOOQ-SRE-DEFAULT_VPC"
}

variable "profile" {
  default = "cino-dev"
}

# Variables for security group
variable "hooq_private_sgname" {
  default = "HooqPrivateSG"
}
variable "hooq_private_sgdesc" {
  default = "Hooq Private SG for DB access"
}
variable "hooq_mysql_port" {
  default = "3306"
}

variable "hooq_redis_port" {
  default = "6379"
}
