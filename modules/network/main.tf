# ------------------------------------------------
#  Module for configuring network subnets
# ------------------------------------------------

terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region = var.region
  profile = var.profile
}

# Creating a VPC for application
resource "aws_vpc" "hooq_vpc" {

  cidr_block = var.hooq_cidr_block

  tags = {
    Name  = var.hooq_vpc_name
  }
}

# Creating one single subnet with no internet access.
resource "aws_subnet" "hooq_private_subnet" {
  vpc_id = aws_vpc.hooq_vpc.id
  cidr_block = cidrsubnet(var.hooq_cidr_block,4,2)
  tags = {
    Name = "${var.hooq_vpc_name}-private-subnet"
  }
}

# Creating security group for private access
resource "aws_security_group" "hooq_private_sg" {
  name        = var.hooq_private_sgname
  description = var.hooq_private_sgdesc
  vpc_id      = aws_vpc.hooq_vpc.id

  ingress {
    # Mysql Access
    from_port   = var.hooq_mysql_port
    to_port     = var.hooq_mysql_port
    protocol    = "tcp"
    # nly allow the private cidr
    cidr_blocks = [var.hooq_cidr_block]
  }
  ingress {
    # Mysql Access
    from_port   = var.hooq_redis_port
    to_port     = var.hooq_redis_port
    protocol    = "tcp"
    # nly allow the private cidr
    cidr_blocks = [var.hooq_cidr_block]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}
