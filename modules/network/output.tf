# -------------------------------------------------
# Define all the output for the module here
# -------------------------------------------------

output "hooq_private_subnet_id"{
  value = aws_subnet.hooq_private_subnet.id
}

output "hooq_private_sg_id" {
  value = aws_security_group.hooq_private_sg.id
}
