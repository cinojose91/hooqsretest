# -----------------------------------------------------
#  Define all the variable required for storage component
# -----------------------------------------------------

variable "profile" {
  default = "cino-dev"
}

variable "region" {
  default = "us-east-1"
}

variable "hooq_s3bucket_name" {
  default = "hooq-sre-test-vpc"
}

variable "hooq_database-instance-identifier" {
  default = "hooq-sre-test-db-instance"
}

variable "hooq_database_intance-class" {
  default = "db.r4.large"
}

variable "hooq_database-cluster-identifier" {
  default =  "hooq-sre-test-db-cluster"
}

variable "hooq_database-cluster-az" {
  type    = list(string)
  default = ["us-east-1a","us-east-1b","us-east-1c"]
}

variable "hooq_database_dbname" {
  default = "hoooq-sre-test-db"
}

variable "hooq_database_username" {
  default = "root"
}

variable "hooq_database_pwd" {
  default = "zxcZXC123!"
}

# Redis variables

variable "hooq_redis-subnet-group" {
  default = "hooq-sre-subnet-group"
}

variable "hooq_private_subnet_id" {
  default = "subnet-123456"
}

variable  "hooq_redis-cluster-name" {
   default = "hooq-sre-test-redis"
}

variable "hooq_redis-engine" {
  default = "redis"
}

variable "hooq_redis-node_type" {
  default = "cache.m4.large"
}

variable "hooq_redis-cache_nodes" {
  default = "1"
}

variable "hooq_redis-pgn" {
  default = "default.redis3.2"
}

variable "hooq_redis-engine-version" {
  default = "3.2.10"
}

variable "hooq_redis-port" {
  default = "6379"
}
