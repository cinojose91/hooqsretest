# ---------------------------------------------------
#  Define all the storage module output here
# ---------------------------------------------------

output "hooq_s3_domain_name" {
  value = aws_s3_bucket.hooq-s3-bucket.bucket_regional_domain_name
}
