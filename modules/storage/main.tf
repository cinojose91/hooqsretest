# -----------------------------------------------------
# Module for all the storage related resources
# -----------------------------------------------------

terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region = var.region
  profile = var.profile
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_s3_bucket" "hooq-s3-bucket" {
  bucket =  var.hooq_s3bucket_name
  acl = "private"
}

# # Aurora database creation
resource "aws_rds_cluster_instance" "hooq_cluster_instances" {
  count              = 2
  identifier         = "${var.hooq_database-instance-identifier}-${count.index}"
  cluster_identifier = aws_rds_cluster.hooq-rds-cluster.id
  instance_class     = var.hooq_database_intance-class
}

# Aurora database cluster setup
resource "aws_rds_cluster" "hooq-rds-cluster" {
  cluster_identifier = var.hooq_database-cluster-identifier
  availability_zones = var.hooq_database-cluster-az
  database_name      = var.hooq_database_dbname
  master_username    = var.hooq_database_username
  master_password    = var.hooq_database_pwd
}

resource "aws_elasticache_subnet_group" "hooq-redis-subnet-group" {
  name       = var.hooq_redis-subnet-group
  subnet_ids = [var.hooq_private_subnet_id]
}

# Elasticache cluster creation
resource "aws_elasticache_cluster" "hooq-redis-cluster" {
  cluster_id           = var.hooq_redis-cluster-name
  engine               = var.hooq_redis-engine
  node_type            = var.hooq_redis-node_type
  num_cache_nodes      = var.hooq_redis-cache_nodes
  parameter_group_name = var.hooq_redis-pgn
  engine_version       = var.hooq_redis-engine-version
  port                 = var.hooq_redis-port
  subnet_group_name    = aws_elasticache_subnet_group.hooq-redis-subnet-group.name
}
