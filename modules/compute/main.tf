# -------------------------------------------------
#  Module for all the compute configuration
# -------------------------------------------------

terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region = var.region
  profile = var.profile
}

# Creating a lambda function
resource "aws_lambda_function" "hooq_backend_lambda" {
   function_name = "HooqBackEndApi"

   # The bucket name as created earlier with "aws s3api create-bucket"
   s3_bucket = var.hooq_lambda_source_s3_bucket
   s3_key    = var.hooq_lambda_source_code

   handler = var.hooq_lambda_handler
   runtime = var.hooq_lambda_runtime
   vpc_config {
    subnet_ids         = [var.hooq_private_subnet_id]
    security_group_ids = [var.hooq_private_security_group]
  }
   role = aws_iam_role.hooq_lambda_exec_role.arn
 }

 # Creating an IAM role for lambda
 resource "aws_iam_role" "hooq_lambda_exec_role" {
   name = "serverless_example_lambda"

   assume_role_policy = file("${path.module}/file/iam_role.json")
 }
 # Allow Lambda role to access rds and cache
 data "aws_iam_policy_document" "hooq_lambda_rds_cache_permission" {
    statement {
        actions = [
            "rds:Describe*",
            "rds:List*",
            "elasticache:*"
        ]
        resources = [
            "arn:aws:rds:::*",
            "arn:aws:elasticache:::*",
        ]
    }
}

# Attach the above policy to the lambda role
resource "aws_iam_role_policy_attachment" "hooq_lambda_rds_cache_perm_attachment" {
    role       = aws_iam_role.hooq_lambda_exec_role.name
    policy_arn = aws_iam_policy.hooq_lambda_rds_cache_permissions.arn
}

 resource "aws_api_gateway_rest_api" "hooq_apigw_restapi" {
  name        = var.hooq_restapi_name
  description = var.hooq_restapi_description
}


resource "aws_api_gateway_resource" "hooq_apigw_proxy" {
   rest_api_id = aws_api_gateway_rest_api.hooq_apigw_restapi.id
   parent_id   = aws_api_gateway_rest_api.hooq_apigw_restapi.root_resource_id
   path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "hooq_apigw_method" {
   rest_api_id   = aws_api_gateway_rest_api.hooq_apigw_restapi.id
   resource_id   = aws_api_gateway_resource.hooq_apigw_proxy.id
   http_method   = "ANY"
   authorization = "NONE"
 }

# Integrate Api GW with lambda
resource "aws_api_gateway_integration" "hooq_apigw_lambdaintegration" {
   rest_api_id = aws_api_gateway_rest_api.hooq_apigw_restapi.id
   resource_id = aws_api_gateway_method.hooq_apigw_method.resource_id
   http_method = aws_api_gateway_method.hooq_apigw_method.http_method

   integration_http_method = "POST"
   type                    = "AWS_PROXY"
   uri                     = aws_lambda_function.hooq_backend_lambda.invoke_arn
 }

# Configuring the root api path
resource "aws_api_gateway_method" "hooq_apigwroot_method" {
   rest_api_id   = aws_api_gateway_rest_api.hooq_apigw_restapi.id
   resource_id   = aws_api_gateway_rest_api.hooq_apigw_restapi.root_resource_id
   http_method   = "ANY"
   authorization = "NONE"
 }

# Root path lambda integration
 resource "aws_api_gateway_integration" "hooq_apigw_lambdaroot_integration" {
   rest_api_id = aws_api_gateway_rest_api.hooq_apigw_restapi.id
   resource_id = aws_api_gateway_method.hooq_apigw_method.resource_id
   http_method = aws_api_gateway_method.hooq_apigw_method.http_method

   integration_http_method = "POST"
   type                    = "AWS_PROXY"
   uri                     = aws_lambda_function.hooq_backend_lambda.invoke_arn
 }

 # API GW deployment
 resource "aws_api_gateway_deployment" "hooq_apigw_deployment" {
   depends_on = [
     aws_api_gateway_integration.hooq_apigw_lambdaintegration,
     aws_api_gateway_integration.hooq_apigw_lambdaroot_integration,
   ]

   rest_api_id = aws_api_gateway_rest_api.hooq_apigw_restapi.id
   stage_name  = var.hooq_apigw_stage_name
 }

 # Allow APIGW to invoke lambda
 resource "aws_lambda_permission" "hooq_apigw_lambda_permission" {
   statement_id  = "AllowAPIGatewayInvoke"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.hooq_backend_lambda.function_name
   principal     = "apigateway.amazonaws.com"

   # The "/*/*" portion grants access from any method on any resource
   # within the API Gateway REST API.
   source_arn = "${aws_api_gateway_rest_api.hooq_apigw_restapi.execution_arn}/*/*"
 }

# Cloudfront configuration
resource "aws_cloudfront_origin_access_identity" "hooq_cf_OAI" {
  comment = "This helps to restrict the s3 pusblic access"
}

locals {
  s3_origin_id = "S3Origin"
  apigw_origin_id = "apigwOrigin"
}

resource "aws_cloudfront_distribution" "hooq_cf_main_distribution" {
  # S3 origin
  origin {
    domain_name = var.hooq_cf-s3-domain-name
    origin_id   = local.s3_origin_id

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.hooq_cf_OAI.cloudfront_access_identity_path
    }
  }
 # Api GW origin
  origin {
	domain_name = replace(aws_api_gateway_deployment.hooq_apigw_deployment.invoke_url, "/^https?://([^/]*).*/", "$1")
	origin_id   = local.apigw_origin_id

	custom_origin_config {
		http_port              = 80
		https_port             = 443
		origin_protocol_policy = "https-only"
		origin_ssl_protocols   = ["TLSv1.2"]
	}
}

# Cache behaviour, the below rule will help all https://app.com/*.js,*.css etc to be forwarded to s3
default_cache_behavior {
	allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
	cached_methods   = ["GET", "HEAD"]
	target_origin_id = local.s3_origin_id

	forwarded_values {
		query_string = false
		cookies {
			forward = "none"
		}
	}

	viewer_protocol_policy = "redirect-to-https"
}

# This is to forward all the api/* traffic to apigw
ordered_cache_behavior {
	path_pattern     = "/api/*"
	allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
	cached_methods   = ["GET", "HEAD"]
	target_origin_id = "apigw"
	default_ttl = 0
	min_ttl     = 0
	max_ttl     = 0

	forwarded_values {
		query_string = true
		cookies {
			forward = "all"
		}
	}
	viewer_protocol_policy = "redirect-to-https"
}
enabled             = true
viewer_certificate {
    cloudfront_default_certificate = true
}
# Enable only SG traffic
restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = ["SG"]
    }
}

}

# Create a DNS record for the CDN

resource "aws_route53_zone" "hooq_primary_domain" {
  name = var.hooq_primary_domain
}

# Create a CNAME for the above created CDN
resource "aws_route53_record" "hooq_primary_cname" {
  zone_id = aws_route53_zone.hooq_primary_domain.zone_id
  name    = var.hooq_primary_cname
  type    = "CNAME"
  ttl     = "300"
  records = [aws_cloudfront_distribution.hooq_cf_main_distribution.domain_name]
}
