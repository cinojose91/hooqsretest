# ------------------------------------------------
#  Define all the outputs for compute module here
# ------------------------------------------------

output "hooq_cdn_domain" {
  value = aws_cloudfront_distribution.hooq_cf_main_distribution.domain_name
}
