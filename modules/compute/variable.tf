# -------------------------------------------------------------
#  Define all the variable required for the compute component
# -------------------------------------------------------------

variable "region" {
  default = "us-east-1"
}

variable "profile" {
  default = "cino-dev"
}

variable "hooq_cf-s3-domain-name" {}

variable "hooq_lambda_source_s3_bucket" {
  default = "hooq-backend-s3-source"
}

# variables for lambda function
variable "hooq_lambda_source_code" {
  default = "1.0/hooqbackend.zip"
}

variable "hooq_lambda_handler" {
  default = "main.handler"
}

variable "hooq_lambda_runtime" {
  default = "nodejs10.x"
}

variable "hooq_restapi_description" {
    default = "Hooq SRE test serverless application"
}

variable "hooq_restapi_name" {
    default = "HOOQSreTestApplication"
}

variable "hooq_apigw_stage_name" {
  default = "dev"
}

variable "hooq_primary_domain" {
  default = "app.com"
}

variable "hooq_primary_cname" {
  default = "www"
}

variable "hooq_private_subnet_id" {
  default = "subnet-123456"
}
variable "hooq_private_security_group" {
  default = "sg-123456x"
}
